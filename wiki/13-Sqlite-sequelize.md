# Database

A database is a collection of information that is organized so that it can be easily accessed, managed and updated. Computer databases typically contain tables that may or may not be related to each other. There are various databases that can be used like MySql, SQLite, Mongo DB, etc.

Since we are creating a signup module, we need to collect the values given by the client and post it to the server. We will be requiring the database to store the information given by the user while signing-up. We will use the **SQLite** database.

## Basic steps for using a database:

- Database connection
- Creating a table structure
- Inserting data into the table
- Modifying and accessing the table data

## SQLite using Sequelize

_SQLite_ is database software that may contain various databases. Here we use _Sequelize_ which is an interface that connects the database like _MySQL_,_SQLite_, etc with the NodeJS application.

In our project, we will use `Sequelize` to create a database connection with SQLite.

Use the code below to create a **`sqlite.js`** file which will be stored in `express/backend/databases/`.

The very first step is to include the Sequelize module by using the following code:

```js
const Sequelize = require("sequelize");
```

Now we will create a database connection using Sequelize.

```js
const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "path_to_database.sqlite"
});
```

`new Sequelize` is a function that creates a new database connection in accordance with the parameters which are given inside this function.

- dialect is used to define which database we are using, whether it is MySQL, SQLite, or MariaDB. Since we are using MySQL, thus `dialect = 'sqlite'`.
- The other parameter is `storage` which is the location of the database.SQLite file which we have created for this workspace.

# Express Setup

We have created a view as `ejs` file. Now we need to connect to the server and make a request to the server. The server must send that created view back to the client as a response.

To use this file and see the demo, save the following code in a file as **`app.js`**.

Let us understand the whole setup using the following code:

The first section of code is to import the important files.

```js
//---------------Importing section----------------------------------------

const express = require("express");
const router = express.Router();
const app = express();
const logger = require("morgan");
const path = require("path");
```

The above code includes various modules and libraries which we must be required to set up a connection with the server. **Each of those modules is explained below when we use them.**

In the code below, Line 1,2,3 and 4 are used to describe and send the _views_ (which we have created) to the server.

`app.set()` is a function that is used to store and retrieve a variable. The first argument is the _key_ and the other argument is the _value_. `app.set()` contains some pre-defined variables like `views`, `view-engine`, `env` etc. to which the value is assigned. The value which is assigned to _views_ is the path where all the _views_ must be kept. In other words, the line of code below indicated that the path to all the views is `__dirname + /client/views`

```js
app.set("views", __dirname + "/client/views"); //line1
```
In the code below, `app.engine()` is a function that takes two arguments. The first argument is an extension (in which a file needs to be rendered), and the other is a callback function with the extension of the EJS template. In the line of code below, it is defined that the `ejs` files need to be sent to the client in the `HTML` format.

**`app.engine()` here will map the EJS template engine to “.html” files**.

```js
// ejs - for rendering ejs in html format
app.engine("html", require("ejs").renderFile); //Line2
```

In the code below, it is defined that the _view engine_ used is the `ejs` format. View engines can be of many types but this `app.set('view engine','ejs')`sets the _view engine_ type to be `ejs`.

```js
// setting view-engine as ejs
app.set("view engine", "ejs"); //Line3
```

In the code below, we have executed the `express.static()` middleware to send the static format. To use this middleware function, we have used the function `app.use()`. `express.static(path.join(__dirname, "client/images/"))` will look for the static content in the specified path and serve it to the client.

```js
app.use(express.static(path.join(__dirname, "client/images/"))); //Line4
```

In the code below, we have used a middleware function called `morgan`. It is used to keep a log of the user whenever a user gets connected to the server. In other words, a log file is a file that records events that occurs either at the client or the server-side. In express, logs can be kept using _morgan_ which itself takes a string argument. In the code below, the argument of the `morgan` middleware function is defined to be `dev` which logs the following parameters: "method","url","status","response time", "size of response". There are various other pre-defined formats other than _dev_ like "common", "combined" etc.

```js
// for logging purposes
app.use(logger("dev")); //Line5
```

Line 6 and 7 in the code below, are used to send the "index.ejs" as response to the client. It is the `ejs` file which we created in **creating views** article.

In the code below, we have created a function which _sends_ the "index.ejs" file in HTML format as response to the client. `res.render()` compiles the 'ejs' view (i.e. provides the value to the variables used in the `ejs` file) and sends it to the client in HTML format.

```js
function home(req, res) {
  //Line6
  res.render("index");
}
```

In the following code, we have 2 functions. One is the middleware function `route()` which is used to define the path to the "index.ejs". The route is defined to be `/`. Using this, whenever the server gets a request, it routes the server response to the "index.ejs" file. The other function is `.get` makes a _get request_ to receive a response. This `get()` request calls the `home` function which itself renders the `index.ejs` in HTML format.

```js
router.route("/").get(home); //Line7
```

The following code is used to specify that the default page which must be sent as response should be the page rendered by `home` function written in _Line6_ above.

```js
app.use("/", home); //Line8
```

In the code below, we have used a function `process.env.PORT`. Every system has some environment variables defined. `process.env.PORT` function looks if the system has defined any port number as its environment variable, then it works on that specific port or it will create a server to listen on port number 4000.

```js
app.set("port", process.env.PORT || 4000); //Line9
```

In the code below, `app.listen` creates a server which listens to the port 4000 or the environment port if available.

```js
app.listen(app.get("port"), () => {
  //Line10
  console.log("Application running in port: " + app.get("port"));
});
```

This is how the whole process works.
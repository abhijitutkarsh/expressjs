# Routing and Rendering the ejs files in HTML format

We created a routing function for sending the "index.ejs" file as response to the client.

```js
function home(req, res) {
  //Line6
  res.render("index");
}
```

But since we want to render several pages, we can create a separate file as **`MainController.js`** which contains the similar rendering function which sends the "index page", "signup page" and "profile page"(which will be sent as response to the client as soon as the client signs up on the server).

The following is the code for `MainController.js`.

```js
module.exports = {
  home: home,
  profile: profile,
  signup: signup
};

function home(req, res) {
  res.render("index");
}

function signup(req, res) {
  res.render("signup");
}

```
**Make a similar logic for `profile()` function by `yourself`**. The above code contains the similar function as we discussed before which sends different views like "index.ejs", "signup.ejs" and "profile.ejs" which we have already created.

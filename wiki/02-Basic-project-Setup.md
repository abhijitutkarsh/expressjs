# Basic Project Setup :

**Note: This is your TASK01, So do follow the steps as mentioned.**

In this project, We will learn to make a request to the server and get an HTML file as a response from the server.

Run the following command:

```
npm install
```
This will install all the required packages

To start with the project, let us make a directory inside **src** folder that will contain the HTML and the js file.

To get inside src folder write below command on terminal
```
$ cd src
```

To make a directory write the below command on terminal

```
$ mkdir express_application
```

Now move into that directory by running the following command.

```
$ cd express_application
```

In this (express_application) directory let us make an HTML file that will be sent as a response to the client whenever the client requests the server.

Run the following command in the terminal to make an HTML file:

```js
touch index.html
```

Now open that file and write the following code inside it :

```html
<html>
 

<head>
        <title>Home page</title>
        <style>
        button {
            padding: 1rem;
            font-size: 1rem;
            font-weight: 600;
            color: #fff;
            background-color: darkblue;
            border: 1px solid darkblue;
            border-radius: 10px;
            letter-spacing: 0.08rem;
        }
    </style>
      </head>
 

<body>
        Happy Coding!!
        <form action="/signup" method="get">
            <button> SignUp </button>
            </form>
      </body>

</html>
```

This HTML page must be sent to the browser as a response when there is any request sent to the server.

So in the next step, let us make another file named `app.js` in the same directory by running the following command in the terminal:

```
$ touch app.js
```
This is known as the **`server file`** of our server. This file will contain the code which will connect to the server and send the above created "index.html" as a response to the client.

To create such a file we need to install certain libraries and store them into our package.json.

Run the following code into the terminal:

```
$ npm install --save express
```

This will install the `express` library and also save it into the package.json file for future use.

Now, let us create a server, by coding inside our `app.js`. Open `app.js` and start coding inside it.

We will first include all the files and libraries which we would be using:

```js
// importing path module
const path = require("path");

// importing express module and assigning into express variable
const express = require("express");

// making an object of the express module. Now 'app' will act as an object of the express.
const app = express();
```

Now our step 1 and step 2 are completed as mentioned in the picture explained in the previous article. Moving to the next step i.e Create a callback function that will send the "index.html" as a response to the client.

The **`res.sendFile()`** is a function that takes an absolute path as an argument and sends that file(in HTML format) as a response to the client.

```js
// sending GET request to the server
// '/' denotes the default page and sending index.html as a response to default page using a callback function

app.get("/", (req, res) => {
  // defining path of our index.html file
  // __dirname (dirname after 2 underscores) provides the path to the current working directory in which this file is stored.
  // Here it will provide the path to an express-application folder so we join our index.html using path.join to it

  let pathToIndexHtml = path.join(__dirname, "index.html");

  // now use pathToIndexHtml and send the file as response. send file takes an absolute path that's why we have done the above step

  res.sendFile(pathToIndexHtml);
});
```

Till now, we have completed 5 steps as mentioned in the picture.
Moving to step 6 i.e. make the server listen to the port.

```js
// defining port number. you can take any port number
let port = 4000;

// make the server listen on this port and displaying some message on the terminal using the callback function

app.listen(port, () => {
  console.log("Application listening on port", port);
});
```

So our final code should look like this :

```js
const path = require("path");
const express = require("express");
const app = express();

app.get("/", (req, res) => {
  let pathToIndexHtml = path.join(__dirname, "index.html");

  res.sendFile(pathToIndexHtml);
});

let port = 4000;

app.listen(port, () => {
  console.log("Application listening on port", port);
});

module.exports = app;
```

Now run this code i.e. your `app.js` to check whether it is working or not use below command :

```
$ node app.js
```

In the above command `node app.js` is the simple basic command that is used to run the js file.

Now use `Ctrl+C` to stop the hosting. But there may be some exceptional cases where you may lose your control from the running application through your terminal and now you have to kill the process working on that port so that port can be used again.

* > `There may be some scenario like if you run your localhost server and have accidentally closed your VsCode, and now you have lost control over your server through the terminal so in that case if you want to run again the same web-application then, it will not run as the required port number had already been acquired by your previous running application. So in that case, this command will help you in finding the` **project ID** `of that application so that you could kill that process forcibly. And be able to run your application again on the same port`.

First, we have to find the process id (pid), for that use below command :

```
$ ps aux | grep node
```

now it will show output like this :

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/terminalPid.png)

Select the pid from the first column referring to `app.js` i.e. in the above case it is `6`. Now kill the process `kill <pid>` command as shown above.

Now go to the root directory and test your task, using given command :

```js
npm run test01
```

Hint:
- To go to the previous directory, use `cd ..`

# Routing and managing the requests:

The next step is to provide the correct path to each request and response. We also need to manage the routing method, i.e., whether it will be done by the _post method_ or the _get method_.

We used a similar function in previous articles where we rendered a static "index.ejs" page.

```js
router.route("/").get(home); //Line7 refer from Express_Setup article
```

Now, since for our application we have numerous routing paths and _get, post_ requests, we can create a separate file named `MainRoutes.js`.

The code below imports various files and libraries which we would be using later.

```js
const express = require("express");
const mainController = require("../controllers/MainController");
const loginController = require("../controllers/LoginController");
const router = express.Router();
const app = express();
```

As we had done before, we will give a path to all the views we had created by using a middleware function `router()` and then we use `.get()` function to render the page we want to send on that path using _get_ request.

In the following line of code, we pass a _get_ request to the `home` function which will render "index.ejs" file on the path `/`. This means whenever the server is connected, it responds to the client by rendering the "index.ejs" page using _get method_ since `.get(mainController.home)` is used.

```js
router.route("/").get(mainController.home); //Line1
```

In the code below, we have defined the path of "signup.ejs" to `/signup`. This means whenever a server is connected, and the user makes a request at `/signup` path, the server responds to the client by sending "signup.ejs" file using _get method_.

```js
router.route("/signup").get(mainController.signup); //Line2
```

In the code below, we have defined the route of "signup.ejs" to `/signup` but also we have made a `post request` to the server which will send the sign-up form data to the server as request.

```js
router.route("/signup").post(loginController.signup); //Line4
```

Since we need to use this routing in another files, we will export the router.

```js
module.exports = router;
```

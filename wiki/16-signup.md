# Creating the SignUp form:

We need a signUp form to send values to the server. As we created a view `index.ejs` in previous articles, similarly we will create a view for SignUp. Make this file in the `views` directory.

```html
<html>
  <head>
    <title>
      Sign Up
    </title>
  </head>
  <body class="text-center">
    <table>
      <form method="post" action="/signup">
        <h1>Please Register</h1>
        <tr>
          <td><label for="inputName">Name</label></td>
          <td>
            <input
              type="text"
              id="inputName"
              name="name"
              placeholder="Enter your name"
              required
              autofocus
            />
          </td>
        </tr>
        <tr>
          <td><label for="inputEmail">Email address</label></td>
          <td>
            <input
              type="email"
              id="inputEmail"
              name="email"
              placeholder="Email address"
              required
              autofocus
            />
          </td>
        </tr>
        <tr>
          <td><label for="inputPassword">Password</label></td>
          <td>
            <input
              type="password"
              id="inputPassword"
              name="password"
              placeholder="Password"
              required
            />
          </td>
        </tr>
        <tr>
          <td>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
              Sign in
            </button>
          </td>
        </tr>
      </form>
    </table>
  </body>
</html>
```

The above code is the simple Html code which creates three fields:

- name
- email
- password

and a **Submit** button.
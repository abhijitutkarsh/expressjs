# Project based on the MVC approach

In this project (TASK 04), we will learn how to use the MVC approach for the basic project that we have done before.

To start with let us understand the architecture that is to be followed. Since we are breaking our code into pieces and making it modular. So we will make some folders and in that, we will keep our files. Below will be the structure used.

```
express-application-mvc
├── app.js
├── backend
│ ├── controller
│ │ └── htmlController.js
│ └── routes
│ │ └── htmlRoutes.js
├── client
│ └── views
│ │ ├── aboutUs.html
│ │ ├── homePage.html
│ │ ├── login.html
│ │ └── signUp.html
├── package.json
└── package-lock.json
```

`express-application-mvc` will be our new directory in which we will do our project using the MVC approach. So make this folder using `mkdir` command.

then inside `express-application-mvc` folder we will create 2 more folders, first one will be `backend` which will contain the files operating at the server-side and second one is `client` which will contain our Html files or the data that is to be shown to the client.

`express-application-mvc` will also contain our `app.js` file that will be the main file.

In the `backend` folder we will create 2 folders one is `controllers` and the other is `routes`.

`controllers` will contain our js files which will be used to serving responses for the client and `routes` will contain js file in which we will map our Html files with url. for eg for `/aboutus` is mapped with aboutUs.html to be sent as a response.

Do setup this architecture on your workspace.

Moving further to codes :

- First, we will make our Html files which will be a view for our client. So we will keep this file in the `client` folder inside the `views` folder.
  Copy the Html files from the previous project or make new ones.

- now let us create a controller for our Html files that will generate a response
  so inside `controllers` folder create a js file named: htmlController.js

open htmlController.js and start coding inside it

```js
// htmlController.js

// importing path module.
const path = require("path");

// serving home page for response

let homePage = (req, res) => {
  // defining path to our index.html file as it is or homepage file.
  let pathToHtml = path.join(__dirname, "../../client/views/homePage.html");
  // sending index.html as a response
  res.sendFile(pathToHtml);
};

// serving about us page for response

let aboutUs = (req, res) => {
  // defining path to aboutUs.html file.
  let pathToHtml = path.join(__dirname, "../../client/views/aboutUs.html");
  // sending the file as a response
  res.sendFile(pathToHtml);
};

// serving about us page for response

let login = (req, res) => {
  // defining path to login.html file.
  let pathToHtml = path.join(__dirname, "../../client/views/login.html");
  // sending the file as a response
  res.sendFile(pathToHtml);
};

// serving about us page for response

let signUp = (req, res) => {
  // defining path to aboutUs.html file.
  let pathToHtml = path.join(__dirname, "../../client/views/signUp.html");
  // sending the file as a response
  res.sendFile(pathToHtml);
};

// exporting our responses.

module.exports = {
  homePage: homePage,
  aboutUs: aboutUs,
  login: login,
  signUp: signUp
};
```

We are done with servicing responses. Now let us define routes to these responses.

So let us move to `routes` folder inside `backend` folder and create a file `htmlRoutes.js`, open this js file and start coding inside it.

```js
// importing express module
const express = require("express");

// importing our controller file so as to map them to their routes
const pages = require("../controller/htmlController");

// express.Router is a middleware
// declaring express router function inside router variable
const router = express.Router();

// mapping index.html as homepage to /
router.route("/").get(pages.homePage);

// mapping aboutUs.html as response to /aboutus url
router.route("/aboutus").get(pages.aboutUs);

// mapping login.html as response to /login url
router.route("/login").get(pages.login);

// mapping aboutUs.html as response to /signup url
router.route("/signup").get(pages.signUp);

// exporting the router.
module.exports = router;
```

Till now we have generated a view and then serviced the response and mapped them to the url. Now its time to make `app.js` file inside `express-application-mvc` folder and start coding inside it.

```js
// importing express module
const express = require("express");

// importing routes that will be serving client
const routes = require("./backend/routes/htmlRoutes");

// defining object of express as app.
const app = express();

// since our responses are mapped to url inside routes. so we will be using routes as a middleware
// app.use() is used to use middleware it will send all the request coming for / to the route. and / will act as default for router and it will map / with it
// if we map /xyz to the routes using app.use then when router will treat /xyz as / for default then to se aboutus page url will be : /xyz/aboutus (TRY IT)
app.use("/", routes);

// creating interaction on port 4000
app.listen(4000, () => {
  console.log("Application listening on port : ", 4000);
});

module.exports = app;
```

Now we are done, we have created views, controllers, routes as middleware. Now its time to test the code.

To run your application run below command

```
$ node app.js
```

Stop the process using below command

```
ps aux | grep node
kill <pid>
```
Now go to the root directory and test your task, using given command :

```js
npm run test04
```

Hint:
- To go to the previous directory, use `cd ..`

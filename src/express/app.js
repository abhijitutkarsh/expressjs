const express = require('express');
const path = require('path');
const logger = require('morgan');
const mainRoutes = require('./backend/routes/MainRoutes');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
app = express();


app.use(cors());
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.set('views', __dirname + '/client/views/');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.static(path.join(__dirname,'client/images/')));

app.use('/', mainRoutes);
app.set('port', process.env.PORT || 5000);

app.listen(app.get('port'), () => {
	console.log('Application listening in port:' + app.get('port'));
})

module.exports = app;
